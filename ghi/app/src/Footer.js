import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

export default function Footer() {
    return (
        <Navbar variant="light" bg="light" fixed="bottom">
            <Container>
            <Navbar.Collapse>
                    <Navbar.Text>
                        <p>Data contained within application is proprietary and confidential.</p>
                    </Navbar.Text>
                </Navbar.Collapse>
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        <p>AutoBahn   |   2022 Z&J Software</p>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
