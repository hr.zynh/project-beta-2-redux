import React from 'react'
import Table from 'react-bootstrap/esm/Table';
import Button from 'react-bootstrap/esm/Button';

export default function SalesRecordList({ salesRecords, deleteSalesRecord }) {
    return (
        <>
            <h1>All Sales Records</h1>
            <Table striped hover>
                <thead>
                    <tr>
                        <th>Sales Representative</th>
                        <th>Customer</th>
                        <th>Vehicle VIN</th>
                        <th>Sale Price</th>
                        <th align='right' width="100px"></th>
                    </tr>
                </thead>
                <tbody>
                    {salesRecords?.map(record => {
                        return (
                            <tr key={record.id}>
                                <td>{record.sales_person.name}</td>
                                <td>{record.customer.name}</td>
                                <td>{record.vin}</td>
                                <td>$ {record.sales_price}</td>
                                <td>
                                    <Button variant="danger" onClick={() => deleteSalesRecord(`${record.id}`)}>Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </>
    )
}
