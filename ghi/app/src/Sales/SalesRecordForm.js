import React, { Component } from 'react'

export default class SalesRepForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            automobile: '',
            sales_person: '',
            customer: '',
            sales_price: "",
            hasEntered: false,
            error: false,
        };

        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handleSalesPriceChange = this.handleSalesPriceChange.bind(this);
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.automobiles;
        delete data.sales_persons;
        delete data.customers;
        delete data.hasEntered;
        delete data.error;

        const createSalesRecord = 'http://localhost:8090/api/sales-records/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const cleared = {
            automobile: "",
            sales_person: "",
            customer: "",
            sales_price: "",
            hasEntered: true,
            error: false,
        }

        const response = await fetch(createSalesRecord, fetchConfig);
        if (response.ok) {
            this.setState(cleared);
            let newRecords = await response.json();
            this.props.addSalesRecord(newRecords.sales_record);
        } else {
            this.setState({
                hasEntered: false,
                error: true,
            })
        }
    }

    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({ automobile: value })
    }

    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({ sales_person: value })
    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({ customer: value })
    }

    handleSalesPriceChange(event) {
        const value = event.target.value;
        this.setState({ sales_price: value })
    }

    render() {
        let successMessageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasEntered) {
            successMessageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
            setTimeout(() => {
                successMessageClasses = 'alert alert-success d-none mb-0';
                formClasses = '';
                this.setState({ hasEntered: false })
            }, 2000);
        }

        let errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
        if (this.state.error) {
            errorMessageClasses = 'alert alert-danger mb-0 mt-3';
            setTimeout(() => {
                errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
                this.setState({ error: false })
            }, 5000);
        }

        return (
            <>
                <div className='my-5 container'>
                    <div className="row">
                        <div className="col">
                            <div className="card shadow">
                                <div className='card-body'>
                                    <h1 className="card-title">Add a New Sales Record</h1>
                                    <p className="mb-3">
                                        Please add a new sales record
                                    </p>
                                    <form className={formClasses} onSubmit={this.handleSubmit} id="create-salesrecord-form">
                                        <div className='col'>
                                            <div className="mb-3">
                                                <select onChange={this.handleAutomobileChange} value={this.state.automobile} required name="automobiles" id="automobiles" className="form-select">
                                                    <option value="">Inventory Vehicle</option>
                                                    {this.props.autos?.map(automobile => {
                                                            return (
                                                                <option key={automobile.vin} value={automobile.vin}>
                                                                    {automobile.vin} - {automobile.year} - {automobile.model.manufacturer.name} - {automobile.model.name} - {automobile.color}
                                                                </option>
                                                            );
                                                        })}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="mb-3">
                                                <select onChange={this.handleSalesPersonChange} value={this.state.sales_person.name} required name="sales_persons" id="sales_persons" className="form-select">
                                                    <option value="">Sales Representative</option>
                                                    {this.props.sales_persons?.map(sales_person => {
                                                        return (
                                                            <option key={sales_person.id} value={sales_person.id}>
                                                                {sales_person.name}
                                                            </option>
                                                        );
                                                    })}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="mb-3">
                                                <select onChange={this.handleCustomerChange} value={this.state.customer} required name="customers" id="customers" className="form-select">
                                                    <option value="">Customer</option>
                                                    {this.props.customers?.map(customer => {
                                                        return (
                                                            <option key={customer.id} value={customer.id}>
                                                                {customer.name}
                                                            </option>
                                                        );
                                                    })}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleSalesPriceChange} value={this.state.sales_price} placeholder="Sales Price" required type="text" name="sales_prices" id="sales_prices" className="form-control" />
                                                <label htmlFor="sales_price">Sale Price (USD)</label>
                                            </div>
                                        </div>
                                        <button className="btn btn-outline-dark">Add Sales Record</button>
                                    </form>
                                    <div className={successMessageClasses} id="success-message">
                                        Sales record has been added!
                                    </div>
                                    <div className={errorMessageClasses} id="error-message">
                                        Entered data not valid!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
