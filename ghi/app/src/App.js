import React from 'react';
import { Component } from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import NavBar from './Nav';
import Footer from './Footer';
import MainPage from './MainPage';
import AppointmentList from './Service/AppointmentList';
import ServiceHistory from './Service/ServiceHistory';
import TechnicianList from './Service/TechnicianList';
import CreateTechnicianForm from './Service/TechnicianForm';
import CreateAppointmentForm from './Service/ServiceForm';
import SalesRecordForm from "./Sales/SalesRecordForm";
import SalesRepForm from "./Sales/SalesRepForm";
import SalesRecordList from "./Sales/SalesRecordList";
import SalesRecordFiltered from "./Sales/SalesRecordFiltered";
import CustomerForm from "./Sales/CustomerForm";
import CustomerList from './Sales/CustomerList';
import ModelsList from "./Inventory/ModelsList";
import ModelsForm from "./Inventory/ModelsForm";
import ManufacturersList from "./Inventory/ManufacturersList";
import ManufacturerForm from "./Inventory/ManufacturerForm";
import VehicleInventory from "./Inventory/VehicleInventory";
import VehicleForm from "./Inventory/VehicleForm";


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.deleteTech = this.deleteTech.bind(this);
    this.deleteSalesRecord = this.deleteSalesRecord.bind(this);
    this.deleteVehicle = this.deleteVehicle.bind(this);
    this.deleteManufacturer = this.deleteManufacturer.bind(this);
    this.deleteModel = this.deleteModel.bind(this);
    this.deleteCustomer = this.deleteCustomer.bind(this);
    this.cancelAppointment = this.cancelAppointment.bind(this);
    this.finishAppointment = this.finishAppointment.bind(this);
    this.addAppointment = this.addAppointment.bind(this);
    this.addCustomer = this.addCustomer.bind(this);
    this.addManufacturer = this.addManufacturer.bind(this);
    this.addModel = this.addModel.bind(this);
    this.addSalesRecord = this.addSalesRecord.bind(this);
    this.addSalesRep = this.addSalesRep.bind(this);
    this.addTechnician = this.addTechnician.bind(this);
    this.addVehicle = this.addVehicle.bind(this);
  }

  async componentDidMount() {
    fetch("http://localhost:8090/api/customers/")
      .then(customers => customers.json())
      .then(customers => this.setState(customers))
    fetch("http://localhost:8090/api/sales-records/")
      .then(sales_records => sales_records.json())
      .then(sales_records => this.setState(sales_records))
    fetch("http://localhost:8100/api/manufacturers/")
      .then(manufacturers => manufacturers.json())
      .then(manufacturers => this.setState(manufacturers))
    fetch("http://localhost:8100/api/models/")
      .then(models => models.json())
      .then(models => this.setState(models))
    fetch("http://localhost:8100/api/automobiles/")
      .then(autos => autos.json())
      .then(autos => this.setState(autos))
    fetch("http://localhost:8080/api/appointments/")
      .then(appointments => appointments.json())
      .then(appointments => this.setState(appointments))
    fetch("http://localhost:8080/api/technicians/")
      .then(technicians => technicians.json())
      .then(technicians => this.setState(technicians))
    fetch('http://localhost:8090/api/sales-persons/')
      .then(sales_persons => sales_persons.json())
      .then(sales_persons => this.setState(sales_persons))
  }

  addAppointment(appointment) {
    this.setState({
      appointments: [...this.state.appointments, appointment]
    })
  }

  addCustomer(customer) {
    this.setState({
      customers: [...this.state.customers, customer]
    })
  }

  addManufacturer(manufacturer) {
    this.setState({
      manufacturers: [...this.state.manufacturers, manufacturer]
    })
  }

  addModel(model) {
    this.setState({
      models: [...this.state.models, model]
    })
  }

  addSalesRecord(record) {
    this.setState({
      sales_records: [...this.state.sales_records, record]
    })
  }

  addSalesRep(person) {
    this.setState({
      sales_persons: [...this.state.sales_persons, person]
    })
  }

  addTechnician(technician) {
    this.setState({
      technicians: [...this.state.technicians, technician]
    })
  }

  addVehicle(vehicle) {
    this.setState({
      autos: [...this.state.autos, vehicle]
    })
  }

  async deleteTech(techid) {
    const technicianUrl = `http://localhost:8080${techid}`;
    const fetchOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(technicianUrl, fetchOptions);
    if (response.ok) {
      this.setState({ technicians: this.state.technicians.filter(tech => tech.href !== techid) });
    };
  };

  async deleteSalesRecord(recordid) {
    const recordUrl = `http://localhost:8090/api/sales-records/${recordid}/`;
    const fetchOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(recordUrl, fetchOptions);
    if (response.ok) {
      this.setState({ sales_records: this.state.sales_records.filter(record => record.id.toString() !== recordid) });
    };
  };

  async deleteVehicle(carid) {
    const vehicleUrl = `http://localhost:8100/api/automobiles/${carid}/`;
    const fetchOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(vehicleUrl, fetchOptions);
    if (response.ok) {
      this.setState({ autos: this.state.autos.filter(car => car.vin !== carid) });
    };
  };

  async deleteManufacturer(manufacturerid) {
    const manufacturerUrl = `http://localhost:8100/api/manufacturers/${manufacturerid}/`;
    const fetchOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(manufacturerUrl, fetchOptions);
    if (response.ok) {
      this.setState({
        manufacturers: this.state.manufacturers.filter(manufacturer => manufacturer.id.toString() !== manufacturerid),
        models: this.state.models.filter(model => model.manufacturer.id.toString() !== manufacturerid),
        autos: this.state.autos.filter(auto => auto.model.manufacturer.id.toString() !== manufacturerid),
      });
    };
  };

  async deleteModel(modelid) {
    const modelUrl = `http://localhost:8100${modelid}`;
    const fetchOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(modelUrl, fetchOptions);
    if (response.ok) {
      this.setState({
        models: this.state.models.filter(model => model.href !== modelid),
        autos: this.state.autos.filter(auto => auto.model.href !== modelid),
      });
    };
  };

  async deleteCustomer(customerid) {
    const customerUrl = `http://localhost:8090/api/customers/${customerid}/`;
    const fetchOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(customerUrl, fetchOptions);
    if (response.ok) {
      this.setState({ customers: this.state.customers.filter(customer => customer.id.toString() !== customerid) });
    };
  };

  async cancelAppointment(appointmentid) {
    const appointmentUrl = `http://localhost:8080${appointmentid}`;
    const fetchOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        active: "False",
        cancelled: "True",
        finished: "False",
      })
    };
    const response = await fetch(appointmentUrl, fetchOptions);
    if (response.ok) {
      this.componentDidMount();
    };
  };

  async finishAppointment(appointmentid) {
    const appointmentUrl = `http://localhost:8080${appointmentid}`;
    const fetchOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        active: "False",
        cancelled: "False",
        finished: "True",
      })
    };
    const response = await fetch(appointmentUrl, fetchOptions);
    if (response.ok) {
      this.componentDidMount();
    };
  };

  render() {
    return (
      <BrowserRouter>
        <NavBar />
        <div className="container" style={{ marginBottom: '100px', marginTop: '20px' }}>
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="sales-persons/">
              <Route path="new/" element={<SalesRepForm addSalesRep={this.addSalesRep} />} />
            </Route>
            <Route path="customers/">
              <Route path="" element={<CustomerList customers={this.state.customers} deleteCustomer={this.deleteCustomer} />} />
              <Route path="new/" element={<CustomerForm addCustomer={this.addCustomer} />} />
            </Route>
            <Route path="sales-records/">
              <Route path="" element={<SalesRecordList salesRecords={this.state.sales_records} deleteSalesRecord={this.deleteSalesRecord} />} />
              <Route path="new/" element={<SalesRecordForm addSalesRecord={this.addSalesRecord} customers={this.state.customers} autos={this.state.autos} sales_persons={this.state.sales_persons} />} />
              <Route path="filter/" element={<SalesRecordFiltered salesRecords={this.state.sales_records} />} />
            </Route>
            <Route path="models/">
              <Route path="" element={<ModelsList vehicleList={this.state.models} deleteModel={this.deleteModel} />} />
              <Route path="new/" element={<ModelsForm addModel={this.addModel} manufacturers={this.state.manufacturers} />} />
            </Route>
            <Route path="manufacturers/">
              <Route path="" element={<ManufacturersList manuList={this.state.manufacturers} deleteManufacturer={this.deleteManufacturer} />} />
              <Route path="new/" element={<ManufacturerForm addManufacturer={this.addManufacturer} />} />
            </Route>
            <Route path="vehicles/">
              <Route path="" element={<VehicleInventory InventoryList={this.state.autos} deleteVehicle={this.deleteVehicle} />} />
              <Route path="new/" element={<VehicleForm models={this.state.models} addVehicle={this.addVehicle} />} />
            </Route>
            <Route path="serviceappointments" >
              <Route path="" element={<AppointmentList appointments={this.state.appointments} cancelAppointment={this.cancelAppointment} finishAppointment={this.finishAppointment} />} />
              <Route path="new" element={<CreateAppointmentForm addAppointment={this.addAppointment} technicians={this.state.technicians} />} />
            </Route>
            <Route path="servicehistory" >
              <Route path="" element={<ServiceHistory appointments={this.state.appointments} />} />
            </Route>
            <Route path="technicians" >
              <Route path="" element={<TechnicianList technicians={this.state.technicians} deleteTech={this.deleteTech} />} />
              <Route path="new" element={<CreateTechnicianForm addTechnician={this.addTechnician} />} />
            </Route>
          </Routes>
        </div>
        <Footer />
      </BrowserRouter>
    );
  };
};
