# Generated by Django 4.0.3 on 2022-10-25 00:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_automobilevinvo_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='appointment',
            options={'verbose_name': 'Service Appointment', 'verbose_name_plural': 'Service Appointments'},
        ),
        migrations.AlterModelOptions(
            name='technician',
            options={'verbose_name': 'Technician', 'verbose_name_plural': 'Technicians'},
        ),
    ]
