import { NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

export default function NavBar() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/">
        <img
              alt=""
              src="/autobahn_logo_white.svg"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
          AutoBahn
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <NavDropdown title="Inventory" id="collasible-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="vehicles/">All Available Vehicles</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="vehicles/new/">Add a New Inventory Vehicle</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="manufacturers/">All Manufacturers</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="manufacturers/new/">Add a New Manufacturer</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="models/">All Available Models</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="models/new/">Add a New Vehicle Model</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Sales" id="collasible-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="customers/">All Customers</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="customers/new/">Add a New Customer</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="sales-records/">All Dealership Sales Records</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="sales-records/new/">Add a New Sales Record</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="sales-records/filter/">Search Sales by Employee</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="sales-persons/new/">Add a New Sales Representative</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Service" id="collasible-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="serviceappointments/">Active Service Appointments</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="serviceappointments/new/">Schedule New Service Appointment</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="servicehistory/">Search Dealership Service History</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="technicians/">Technician List</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="technicians/new/">Add a New Technician</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
