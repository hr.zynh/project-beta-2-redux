import React from 'react'
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

export default function VehicleInventory({ InventoryList, deleteVehicle }) {
    return (
        <>
            <h1>Vehicle Inventory</h1>
            <Table striped hover>
                <thead>
                    <tr>
                        <th>Vehicle Inventory</th>
                        <th>Year</th>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th align='right' width="100px"></th>
                    </tr>
                </thead>
                <tbody>
                    {InventoryList && InventoryList.map(vehicle => {
                        return (
                            <tr key={vehicle.id}>
                                <td>{vehicle.vin}</td>
                                <td>{vehicle.year}</td>
                                <td>{vehicle.model.manufacturer.name}</td>
                                <td>{vehicle.model.name}</td>
                                <td>{vehicle.color}</td>
                                <td>
                                    <Button variant="danger" onClick={() => deleteVehicle(`${vehicle.vin}`)}>Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </>
    )
}
