import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/esm/Button';

export default function ManufacturersList({ manuList, deleteManufacturer }) {
    return (
        <>
            <h1>Available Vehicle Manufacturers</h1>
            <Table striped hover>
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th align='right' width="100px"></th>
                    </tr>
                </thead>
                <tbody>
                    {manuList && manuList.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                                <td>
                                    <Button variant="danger" onClick={() => deleteManufacturer(`${manufacturer.id}`)}>Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </>
    )
}
