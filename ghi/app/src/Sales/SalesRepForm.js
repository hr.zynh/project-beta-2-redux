import React, { Component } from 'react'

export default class SalesRepForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employeeNumber: '',
            hasEntered: false,
            error: false,
        }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.employee_number = data.employeeNumber;
        delete data.employeeNumber;
        delete data.hasEntered;
        delete data.error;

        const createSalesPerson = 'http://localhost:8090/api/sales-persons/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const cleared = {
            name: '',
            employeeNumber: '',
            hasEntered: true,
            error: false,
        }

        const response = await fetch(createSalesPerson, fetchConfig);
        if (response.ok) {
            this.setState(cleared);
            let newRep = await response.json()
            this.props.addSalesRep(newRep);
        } else {
            this.setState({
                hasEntered: false,
                error: true,
            })
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({ employeeNumber: value });
    }

    render() {
        let successMessageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasEntered) {
            successMessageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
            setTimeout(() => {
                successMessageClasses = 'alert alert-success d-none mb-0';
                formClasses = '';
                this.setState({ hasEntered: false })
            }, 2000);
        }

        let errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
        if (this.state.error) {
            errorMessageClasses = 'alert alert-danger mb-0 mt-3';
            setTimeout(() => {
                errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
                this.setState({ error: false })
            }, 5000);
        }

        return (
            <>
                <div className='my-5 container'>
                    <div className="row">
                        <div className="col">
                            <div className="card shadow">
                                <div className='card-body'>
                                    <h1 className="card-title">Add a New Sales Representative</h1>
                                    <p className="mb-3">
                                        Please add a new sales representative
                                    </p>
                                    <form className={formClasses} onSubmit={this.handleSubmit} id="create-salerep-form">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                                <label htmlFor="name">Employee Name</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleEmployeeNumberChange} value={this.state.employeeNumber} placeholder="Employee Number" required type="number" name="employeeNumber" id="employeeNumber" className="form-control" />
                                                <label htmlFor="employeeNumber">Employee No.</label>
                                            </div>
                                        </div>
                                        <button className="btn btn-outline-dark">Add Sales Representative</button>
                                    </form>
                                    <div className={successMessageClasses} id="success-message">
                                        Sales representative has been added!
                                    </div>
                                    <div className={errorMessageClasses} id="error-message">
                                        Entered data not valid!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
