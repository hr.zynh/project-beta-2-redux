import React, { useState } from 'react'
import Table from 'react-bootstrap/Table';

export default function SalesRecordFiltered({ salesRecords }) {

    const [search, setSearch] = useState("");
    return (
        <>
            <h1>Sales Representative History</h1>
            <div className="container" style={{ marginTop: '20px' }}>
                <div className="pb row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                        <div className="form-group">
                            <div className="input-group">
                                <input onChange={event => setSearch(event.target.value)} className="form-control" type="text" placeholder="Search Sales Records by Sales Representative" />
                            </div>
                        </div>
                    </form>
                </div>

                <Table striped hover style={{ marginTop: '20px' }}>
                    <thead>
                        <tr>
                            <th>Sales Representative</th>
                            <th>Customer</th>
                            <th>Vehicle VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesRecords?.filter(record => record.sales_person.name.toLowerCase().includes(search.toLowerCase())).map(record => {
                            return (
                                <tr key={record.id}>
                                    <td>{record.sales_person.name}</td>
                                    <td>{record.customer.name}</td>
                                    <td>{record.vin}</td>
                                    <td>$ {record.sales_price}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
        </>
    )
}
