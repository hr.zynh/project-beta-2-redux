import React, { Component } from 'react'

export default class ManufacturerForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            hasEntered: false,
            error: false,
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.hasEntered;
        delete data.error;

        const createManufacturer = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const cleared = {
            name: '',
            hasEntered: true,
            error: false,
        }

        const response = await fetch(createManufacturer, fetchConfig);
        if (response.ok) {
            this.setState(cleared);
            let newManufacturer = await response.json();
            this.props.addManufacturer(newManufacturer);
        } else {
            this.setState({
                hasEntered: false,
                error: true,
            })
        }
    }


    render() {
        let successMessageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasEntered) {
            successMessageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
            setTimeout(() => {
                successMessageClasses = 'alert alert-success d-none mb-0';
                formClasses = '';
                this.setState({ hasEntered: false })
            }, 2000);
        }

        let errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
        if (this.state.error) {
            errorMessageClasses = 'alert alert-danger mb-0 mt-3';
            setTimeout(() => {
                errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
                this.setState({ error: false })
            }, 5000);
        }

        return (
            <>
                <div className='my-5 container'>
                    <div className="row">
                        <div className="col">
                            <div className="card shadow">
                                <div className="card-body">
                                    <h1>Add a New Manufacturer</h1>
                                    <p className="mb-3">
                                        Please add a new manufacturer (note: must be unique)
                                    </p>
                                    <form className={formClasses} onSubmit={this.handleSubmit} id="create-salerep-form">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
                                                <label htmlFor="name">Manufacturer Name</label>
                                            </div>
                                        </div>
                                        <button className="btn btn-outline-dark">Add Manufacturer</button>
                                    </form>
                                    <div className={successMessageClasses} id="success-message">
                                        Manufacturer has been added!
                                    </div>
                                    <div className={errorMessageClasses} id="error-message">
                                        Entered data not valid!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
