import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/esm/Button';

export default function ModelsList({ vehicleList, deleteModel }) {
    return (
        <>
            <h1>Available Vehicle Models</h1>
            <Table striped hover>
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th className="text-center">Picture</th>
                        <th align='right' width="100px"></th>
                    </tr>
                </thead>
                <tbody>
                    {vehicleList && vehicleList.map(model => {
                        return (
                            <tr key={model.href}>
                                <td>{model.manufacturer.name}</td>
                                <td>{model.name}</td>
                                <td className='text-center'><img src={model.picture_url} alt="" width="400px" height="auto" /></td>
                                <td>
                                    <Button variant="danger" onClick={() => deleteModel(`${model.href}`)}>Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </>
    )
}
