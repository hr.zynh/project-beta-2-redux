import React, { Component } from 'react'

export default class CustomerForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            address: '',
            phoneNumber: '',
            hasEntered: false,
            error: false,
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.phone_number = data.phoneNumber
        delete data.phoneNumber;
        delete data.hasEntered;
        delete data.error;

        const customerURL = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const cleared = {
            name: '',
            address: '',
            phoneNumber: '',
            hasEntered: true,
            error: false,
        }

        const response = await fetch(customerURL, fetchConfig);
        if (response.ok) {
            this.setState(cleared);
            let newCustomer = await response.json()
            this.props.addCustomer(newCustomer)
        } else {
            this.setState({
                hasEntered: false,
                error: true,
            })
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({ address: value });
    }

    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({ phoneNumber: value });
    }

    render() {
        let successMessageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasEntered) {
            successMessageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
            setTimeout(() => {
                successMessageClasses = 'alert alert-success d-none mb-0';
                formClasses = '';
                this.setState({ hasEntered: false })
            }, 2000);
        }

        let errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
        if (this.state.error) {
            errorMessageClasses = 'alert alert-danger mb-0 mt-3';
            setTimeout(() => {
                errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
                this.setState({ error: false })
            }, 5000);
        }

        return (
            <>
                <div className="my-5 container">
                    <div className="row">
                        <div className="col">
                            <div className="card shadow">
                                <div className="card-body">
                                    <h1 className="card-title">Add a New Customer</h1>
                                    <p className="mb-3">
                                        Please add a new customer
                                    </p>
                                    <form className={formClasses} onSubmit={this.handleSubmit} id="create-customer-form">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                                <label htmlFor="name">Name</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleAddressChange} value={this.state.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                                                <label htmlFor="address">Address</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handlePhoneNumberChange} value={this.state.phoneNumber} placeholder="Phone No." required type="text" name="phoneNumber" id="phoneNumber" className="form-control" />
                                                <label htmlFor="phoneNumber">Phone No.</label>
                                            </div>
                                        </div>
                                        <button className="btn btn-outline-dark">Add Customer</button>
                                    </form>
                                    <div className={successMessageClasses} id="success-message">
                                        Customer has been added!
                                    </div>
                                    <div className={errorMessageClasses} id="error-message">
                                        Entered data not valid!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
